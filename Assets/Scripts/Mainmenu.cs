﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Mainmenu : MonoBehaviour {

    public GameObject screen1, screen2, classicMaps,risingMaps;
    private int gamemode;

    public void classicMode()
    {
        gamemode = 0;
        screen2.SetActive(false);
        classicMaps.SetActive(true);
    }

    public void classicMap1()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    public void classicMap2()
    {
        SceneManager.LoadScene("borderlessClassicMap1", LoadSceneMode.Single);
    }

    public void boarderlessMode()
    {
        SceneManager.LoadScene("NotContained", LoadSceneMode.Single);
    }

    public void risingMode()
    {
        gamemode = 1;
        screen2.SetActive(false);
        risingMaps.SetActive(true);
    }

    public void risingMap1()
    {
        SceneManager.LoadScene("RisingMap1",LoadSceneMode.Single);
    }

    public void backFromMaps()
    {
        switch (gamemode)
        {
            case 0:
                classicMaps.SetActive(false);
                screen2.SetActive(true);
                break;
            case 1:
                risingMaps.SetActive(false);
                screen2.SetActive(true);
                break;
        }
    }

    public void play()
    {
        screen1.SetActive(false);
        screen2.SetActive(true);
    }

    public void exit()
    {
        Application.Quit();
    }
}
