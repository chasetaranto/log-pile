﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinDetect : MonoBehaviour {

    public GameObject p1, p2;
    public Canvas winScreen;
    private bool winDeclared;
    public Text winDeclare;
    public Sprite bkd1, bkd2,bkd3,bkd4,bkd5;
    public SpriteRenderer bkd;

	// Use this for initialization
	void Start () {
        winDeclared = false;
        System.Random rNum = new System.Random();
        switch (rNum.Next(0, 5))
        {
            case 0:
                bkd.sprite = bkd1;
                break;
            case 1:
                bkd.sprite = bkd2;
                break;
            case 2:
                bkd.sprite = bkd3;
                break;
            case 3:
                bkd.sprite = bkd4;
                break;
            case 4:
                bkd.sprite = bkd5;
                break;
        }
        
	}
	
	// Update is called once per frame
	void Update () {
	    if (Win.winner != 0 && !winDeclared )
        {
            Debug.Log(Win.winner);
            winDeclared = true;
            //replayBttn.gameObject.SetActive(true);
            if (Win.winner == 1)
            {
                winDeclare.text = "Player 1\nWins";
                winScreen.gameObject.SetActive(true);
            }
            else
            {
                winDeclare.text = "Player 2\nWins";
                winScreen.gameObject.SetActive(true);
            }
        }
        if (SceneManager.GetActiveScene().name == "NotContained")
        {
            if (p1.transform.position.y < -30)
            {
                p1.SetActive(false);
                Win.winner = 2;
            }
            if (p2.transform.position.y < -20)
            {
                p2.SetActive(false);
                Win.winner = 1;
            }
        }

	}
    public void replayBttnPress()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex,LoadSceneMode.Single);
        Win.winner = 0;
    }

	public void menuBttnPress()
	{
        SceneManager.LoadScene("Mainmenu");
        Win.winner = 0;
	}
}
public class Win
{
    public static int winner=0;
}
