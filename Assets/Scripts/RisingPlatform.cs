﻿using UnityEngine;

public class RisingPlatform : MonoBehaviour {
    System.Random rNum;

	// Use this for initialization
	void Start () {
        rNum = new System.Random();
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnBecameInvisible()
    {
        Debug.Log("resetting");
        transform.position = new Vector3(rNum.Next(-15,15), 12, 0);
    }
}
