﻿using UnityEngine;

public class player1Move : MonoBehaviour {
    private Rigidbody rb;
    private bool touchingLevel, jump, rLeft, rRight, winDecided;
    private int jumpForce, torqueForce;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jump = false;
        rLeft = false;
        rRight = false;
        winDecided = false;
        jumpForce = 500;
        torqueForce = 50;
    }

    void FixedUpdate()
    {
        if (jump)
        {
            rb.AddRelativeForce(new Vector3(0, 0, jumpForce));
            //touchingLevel = false;
        }
        if (transform.gameObject.name == "Player1")
        {
            if (rRight)
            {
                rb.AddRelativeTorque(new Vector3(-torqueForce, 0, 0));
            }
            if (rLeft)
            {
                rb.AddRelativeTorque(new Vector3(torqueForce, 0, 0));
            }
        }
        else if (transform.gameObject.name == "Player2")
        {
            if (rRight)
            {
                rb.AddRelativeTorque(new Vector3(torqueForce, 0, 0));
            }
            if (rLeft)
            {
                rb.AddRelativeTorque(new Vector3(-torqueForce, 0, 0));
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (transform.gameObject.name == "Player1")
        {
            if (Input.GetKeyDown(KeyCode.W) && touchingLevel)
            {
                jump = true;
            }
            else
            {
                jump = false;
            }
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D))
            {
                rRight = true;
            }
            else if (Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A))
            {
                rLeft = true;
            }
            else
            {
                rLeft = false;
                rRight = false;
            }
            if (transform.position.y < -15)
            {
                Win.winner = 2;
                transform.position =new Vector3(0,0,0);
                transform.gameObject.SetActive(false);
            }
        }
        if (transform.gameObject.name == "Player2")
        {
            {
                if (Input.GetKeyDown(KeyCode.UpArrow) && touchingLevel)
                {
                    jump = true;
                }
                else
                {
                    jump = false;
                }
                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKey(KeyCode.RightArrow))
                {
                    rRight = true;
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKey(KeyCode.LeftArrow))
                {
                    rLeft = true;
                }
                else
                {
                    rLeft = false;
                    rRight = false;
                }
            }
        }
    }

    void OnBecameInvisible()
    {
        transform.gameObject.SetActive(false);
        if ((transform.gameObject.name == "Player2"))
        {
            Win.winner = 1;
        }
        if ((transform.gameObject.name == "Player1"))
        {
            Win.winner = 2;
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.CompareTag("Level"))
        {
            touchingLevel = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("Level"))
        {
            touchingLevel = false;
            //OnCollisionEnter(transform.gameObject.GetComponent<Collision>());
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Level"))
        {
            touchingLevel = true;
        }
        else
        {
            Debug.Log(col.gameObject.name);
        }
        /*if (col.gameObject.name == "Player1" || col.gameObject.name == "Player2")
        {
            Debug.Log("playersTouch");
            if (col.gameObject.name == "Player1")
            {
                Win.winner = 1;
            }
            else if (col.gameObject.name == "Player2")
            {
                Win.winner = 2;
            }
        }*/
    }
}
