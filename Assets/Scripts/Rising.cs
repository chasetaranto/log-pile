﻿using UnityEngine;

public class Rising : MonoBehaviour {
    public int speed;
    public GameObject platforms;
    private int platformCount;

    void changeYBy(GameObject item,float value)
    {
        item.transform.position = new Vector3(item.transform.position.x, item.transform.position.y + value, item.transform.position.z);
    }

	// Use this for initialization
	void Start () {
        platformCount = platforms.transform.childCount;
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0;i<platformCount;i++)
        {
            changeYBy(platforms.transform.GetChild(i).gameObject, -Time.deltaTime*speed);
        }
	}
}
